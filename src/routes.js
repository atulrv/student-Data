import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/app';
import Dashboard from './components/Dashboard/Dashboard';
import DetailsViewPage from './components/DetailsView/DetailsViewPage';


export default (
    <Route path="/" component={App}>
        <IndexRoute  component={Dashboard}/>
        <path path="DetailsViewPage" component={DetailsViewPage} />
    </Route>
);