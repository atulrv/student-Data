import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';
import PreLoader from '../common/loader';
import {Link} from 'react-router';

class DetailViewPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.eventDataInfo = '';
    }

    componentDidMount() {
        this.props.actionMaster.EndLoader();



    }

    goToHome = () => {
        this.props.actionMaster.goHome()
    }



    render() {
        console.log("this.props.studentData",this.props.studentData)
        return (
            <div>
                <Header onFilter={this.onFilterData} sortData={this.sortingData} sortDataAccToMarks={this.shortingDataForMarks} onHide={true}/>

                <div className="container">
                    <div className="row">

                        {
                            this.props.preLoader === true ?
                                <div className="loaderH">
                                    <PreLoader/>
                                </div>:
                                null
                        }

                                <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div className="box">
                                        <div className="box-icon">
                                            <span className="fa fa-4x fa-html5"></span>
                                        </div>
                                        <div className="info">
                                            <h4 className="text-center">{this.props.selectedData.name !== undefined ?this.props.selectedData.name : null}</h4>
                                               <p><span>Class : </span>{this.props.selectedData.class}</p>
                                               <p><span>Roll No : </span>{this.props.selectedData.rollNo}</p>
                                               <p><span>Marks in S1 : </span>{this.props.selectedData.marks !== undefined ? this.props.selectedData.marks.s1 : null}</p>
                                               <p><span>Marks in S2 : </span>{this.props.selectedData.marks !== undefined ? this.props.selectedData.marks.s2 : null}</p>
                                               <p><span>Marks in S3 : </span>{this.props.selectedData.marks !== undefined ? this.props.selectedData.marks.s3:null}</p>
                                               <p><span>Total Marks : </span>{this.props.selectedData.totalMarks}</p>
                                            <a href="" className="btn" onClick={this.goToHome}>Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


        );
    }

}

DetailViewPage.propTypes = {
    actionMaster: PropTypes.object,
    selectedData:PropTypes.object
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailViewPage);
