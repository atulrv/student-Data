import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';

class Header extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {

    }

    sortAcctoAlphabet = () =>{
      this.props.sortData()
    };
    sortAcctoMarksreverse = () =>{
      this.props.sortDataAccToMarks()
    };

    handleFilter = (event) => {
        this.props.onFilter(event.target.value);
    };



    render() {
        return (
            <nav className="navbar navbar-inverse navlook">
                <div className="container">
                    <div className="navbar-header">
                    </div>
                    <form className="navbar-form navbar-left" >
                        {
                            this.props.onHide === true ?
                                null:
                                <div>
                                    <div className="form-group">
                                        <input type="text" className="form-control" onChange={this.handleFilter} placeholder="Search"/>
                                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="#" className="btn btn-info btn-lg sortButton" onClick={this.sortAcctoAlphabet} >
                                        <span className="glyphicon glyphicon-chevron-up" ></span> Sort
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="#" className="btn btn-info btn-lg sortButton" onClick={this.sortAcctoMarksreverse}>
                                        <span className="glyphicon glyphicon-chevron-down"></span> down
                                    </a>
                                </div>
                        }

                    </form>
                </div>
            </nav>
        );
    }

}

Header.propTypes = {
    actionMaster: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
