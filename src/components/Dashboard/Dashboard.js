import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';
import PreLoader from '../common/loader';

class Dashboard extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.eventDataInfo = '';
    }

    componentDidMount() {
       this.props.actionMaster.startLoader();
        setTimeout(function () {
            this.props.actionMaster.getStudentData();

        }.bind(this),2000)


    }

    onFilterData = (d) => {
        this.eventDataInfo = d;
        console.log("this.eventDataInfo",this.eventDataInfo)
        this.forceUpdate()
       // this.props.actionMaster.onDataFilterFromStudent(d)
    };

    sortingData = () => {
        this.props.actionMaster.sortData();
        //this.forceUpdate()
    };
    shortingDataForMarks = () => {
        this.props.actionMaster.sortDataAccToMark();
        //this.forceUpdate()
    };
    onMove = (data) => {
        this.props.actionMaster.startLoader();
        setTimeout(function () {
            this.props.actionMaster.onGoToDetail(data);
        }.bind(this),500)
    };

    render() {
       console.log("this.props.studentData",this.props.studentData)
        return (
            <div>
                <Header onFilter={this.onFilterData} sortData={this.sortingData} sortDataAccToMarks={this.shortingDataForMarks}/>

            <div className="container">
                <div className="row">

                    {
                        this.props.preLoader === true ?
                            <div className="loaderH">
                                <PreLoader/>
                            </div>:
                            null
                    }


                    {
                        this.props.studentData !== null || this.props.studentData !== undefined ?
                        this.props.studentData.map((data,i)=>{
                            //console.log("57",data)
                            if(data.name !== null && data.name !== undefined ){

                            if(this.eventDataInfo !== '' ){
                                if(data.name.includes(this.eventDataInfo)){
                                    return<div className="col-sm-6 col-lg-4"  onClick={() => this.onMove(data)}>
                                        <div className="panel panel-primary">
                                            <div className="panel-heading">{data.name}</div>
                                            <div className="panel-body">
                                                <div className="panel-footer">Roll No.<span>{ data.rollNo}</span></div>
                                                <div className="panel-footer">Total Marks<span>{data.marks.s1+data.marks.s2+data.marks.s3}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                            }else {
                                return<div className="col-sm-6 col-lg-4" onClick={() => this.onMove(data)}>
                                    <div className="panel panel-primary">
                                        <div className="panel-heading">{data.name}</div>
                                        <div className="panel-body">
                                            <div className="panel-footer">Roll No.<span>{ data.rollNo}</span></div>
                                            <div className="panel-footer">Total Marks<span>{data.marks.s1+data.marks.s2+data.marks.s3}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }
                            }


                        }):
                            <div>No Data!</div>
                    }

                </div>
            </div>
            </div>

        );
    }

}

Dashboard.propTypes = {
    actionMaster: PropTypes.object,
    studentData:PropTypes.object
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
