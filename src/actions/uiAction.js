import studentApi from "../api/studentApi";


export function fetchedStudentData(data) {
    return{type:"FETCHED_STUDENT_DATA",data};
}

export function sortData(data) {
    return{type:"SORT_DATA",data};
}
export function sortDataAccToMark(data) {
    return{type:"SORT_DATA_ACC_TO_MARKS",data};
}

export function onGoToDetail(data) {
    return{type:"ON_GOTO_DETAIL",data};
}

export function onError(data) {
    return{type:"ON_ERROR",data};
}

export function goHome(data) {
    return{type:"GO_HOME",data};
}


export function startLoader() {
    return{type:"START_LOADER"}
}
export function EndLoader() {
    return{type:"END_LOADER"}
}

export function getStudentData() {
    return function (dispatch) {
        setTimeout(function () {
            studentApi.fetchStudentData().then(data =>{
                return dispatch (fetchedStudentData(data));
            }).catch(error => {
                return dispatch(onError(error));
            });
        });

    };
}