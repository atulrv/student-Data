import {hashHistory} from 'react-router';
import initialState from './initialState';
import $ from 'jquery';
import toastr from 'toastr';


export default function applicationReducers(state = initialState, action) {

    switch (action.type) {

        case 'START_LOADER': {

            const newState = JSON.parse(JSON.stringify(state));
            newState.preLoader = true;
            return newState;
        }
        case 'END_LOADER': {

            const newState = JSON.parse(JSON.stringify(state));
            newState.preLoader = false;
            return newState;
        }
        case 'ON_GOTO_DETAIL': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.selectedData = action.data;
            console.log("newState.selectedData",newState.selectedData);
            setTimeout(function () {
                hashHistory.push("DetailsViewPage");
            }, 0);
            return newState;
        }
        case 'GO_HOME': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.selectedData = {};
            console.log("GO_HOME",newState.selectedData);
            setTimeout(function () {
                hashHistory.push("/");
            }, 0);
            return newState;
        }

        case 'FETCHED_STUDENT_DATA': {

            const newState = JSON.parse(JSON.stringify(state));
            newState.cc = []
            for(let j = 0 ; j<action.data.length;j++){
                if(action.data[j] !== undefined){
                    action.data[j].totalMarks = action.data[j].marks.s1 + action.data[j].marks.s2 + action.data[j].marks.s3
                    newState.cc.push(action.data[j]);
                }
            }
            newState.studentData = newState.cc;
            newState.preLoader = false;
            console.log("FETCHED_STUDENT_DATA",newState.studentData)
            return newState;
        }
        case 'SORT_DATA': {

            const newState = JSON.parse(JSON.stringify(state));
            if(newState.studentData !== null || newState.studentData !== undefined ){
                if(newState.toogleClicked === false){
                    newState.toogleClicked = true;
                    newState.studentData.sort()

                }else {
                    newState.toogleClicked =false
                    newState.studentData =  newState.studentData.reverse();
                }
            }

            console.log("SORT_DATA",newState.studentData)

            return newState;
        }
        case 'SORT_DATA_ACC_TO_MARKS': {

            const newState = JSON.parse(JSON.stringify(state));
            if(newState.studentData !== null || newState.studentData !== undefined ){
                if(newState.toogleClickedForMarks === false){
                    console.log("if")
                    for(let h = 0 ; h< newState.studentData.length;h++ ){
                        if(newState.studentData[h].totalMarks === newState.studentData[h].marks.s1+ newState.studentData[h].marks.s2+ newState.studentData[h].marks.s3){
                            console.log("test")
                        }else {
                            newState.studentData.sort((a, b) =>  parseInt(a.totalMarks) - parseInt(b.totalMarks));

                        }
                    }
                    newState.toogleClickedForMarks = true;

                }else if(newState.toogleClickedForMarks === true){
                    console.log("else")
                    newState.toogleClickedForMarks = false;
                    newState.studentData =  newState.studentData.reverse();
                }
            }

            console.log("SORT_DATA_ACC_TO_MARKS",newState.studentData)

            return newState;
        }


        case 'ON_ERROR' : {
            const newState = JSON.parse(JSON.stringify(state));

            newState.preLoader = false;
            return newState;

        }

        default:
            return state;
    }
}