import {combineReducers} from 'redux';
import {reducer as reduxFormReducer } from "redux-form";
import application from './applicationReducers';

const rootReducer = combineReducers({
    application,
    form: reduxFormReducer
});

export default rootReducer;
